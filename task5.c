#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdbool.h>

#define MEALS 5 //define meals
#define PHILOSOPHERS 5 //define philosophers

int phil_ID, sem_ID, child_pid, status;

void grab_forks(int left_fork_id)
{
    int right_fork_id = (left_fork_id + 1) % PHILOSOPHERS;
    struct sembuf operation[2] = { {left_fork_id,-1,0}, {right_fork_id,-1,0} }; //create structure by using sembuf and decrement by 1
    semop(sem_ID, operation, 2); //semaphore operation which stores details of meals and philosophers
    printf("Philosopher %d grabs forks %d and %d\n", phil_ID, left_fork_id, right_fork_id);

}

void put_away_forks(int left_fork_id)
{
    int right_fork_id = (left_fork_id + 1) % PHILOSOPHERS;
    struct sembuf operation[2] = {  {left_fork_id,1,0}, {right_fork_id,1,0} }; //create structure array and increment by 1
    semop(sem_ID, operation, 2); //semaphore operation which stores details of meals and philosophers
    printf("Philosopher %d puts away forks %d and %d\n", phil_ID, left_fork_id, right_fork_id);
}

void eat(int num_of_meals)
{
    grab_forks(phil_ID);
    printf("\nPhilosopher %d is eating meal %d\n", phil_ID, (MEALS-num_of_meals));
    sleep(1);
    put_away_forks(phil_ID);
}

void think()
{
    printf("Philosopher %d is thinking\n", phil_ID);
    sleep(2);
}

int main()
{
	printf("All Philosophers sitting at the table\n");

    	// semaphore allocation
	sem_ID = semget(IPC_PRIVATE, PHILOSOPHERS, IPC_CREAT | 0644);
	//A new set of nsems semaphores is created
    if (sem_ID == -1)// if failure of creation
    {
        perror("\nFailed to create semaphore\n"); //print the error
        exit(1);
    }

    for (int i = 0; i < PHILOSOPHERS; i++)
	semctl(sem_ID, i, SETVAL, 1);
    // creating child processes - philosophers
    for (int i = 0; i < PHILOSOPHERS; i++)
    {
        child_pid = fork();
        if (child_pid == 0) // child process
        {
            phil_ID = i;
            //printf("philosopher %d: process started\n", phil_ID);
            int meals = MEALS;
            bool isHungry = 0;
            while (meals)
            {
                if (isHungry)
                {
		    meals--;
                    eat(meals);
                    isHungry = 0;
                }
                else
                {
                    think();
                    isHungry = 1;
                }
            }
            return 0;
        }
        else if (child_pid==-1) //failure of creation
        {
            printf("Failure of creation\n");
            kill(-getpgrp(),SIGTERM);//terminate all the processes in group
            exit(1);
        }
    }
    while (1)
    {
        child_pid = wait(&status);
        if (child_pid < 0) // terminate child processes
            break;
    }
    // Destroying semaphores
    if (semctl (sem_ID, 0, IPC_RMID) == -1)
      {
	printf("Failure in deleting set of semaphores\n");
	}
    else
       {
	printf("\nAll Philosophers on the table ate succesfully\n");
	}

    return 0;
}

