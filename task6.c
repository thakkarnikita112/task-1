#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
/* function for copying using read/write */
void copy_read_write(int inputFile,int outputFile)
{
    printf("copying using read/write..\n");
	int dataRead;
	if(inputFile >0)
	{
		// there are things to read from the input
		char buffer[128] = {0};
		do
		{
			
			/* read data in local buffer */
			dataRead = read(inputFile, buffer, sizeof(buffer));
			//printf("%s\n", buffer);
			/* write the data to output file */
			write(outputFile, buffer, dataRead);
		}while(dataRead>0);
		/* close the input file when all data is copied */
		close(inputFile);
	}
	/* close the output file */
	close(outputFile);
	printf("copy done.\n");
}

/* function for copying using mmap */
void copy_mmap(int inputFile,int outputFile)
{
	printf("copying using mmap..\n");
	size_t filesize;
	/* find the size of file */
	filesize = lseek(inputFile, 0, SEEK_END);
	char *source, *destination;
	/* map the source file in virtual address space */
	source = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, inputFile, 0);
	/* trunccate to fit */
	ftruncate(outputFile, filesize);
	/* map the destination file into process address space */
	destination = mmap(NULL, filesize, PROT_READ | PROT_WRITE, MAP_SHARED, outputFile, 0);
	/* both memories are set up .s o now copy both the files */

	memcpy(destination, source, filesize);

	/* unmap the files */
	munmap(source, filesize);
	munmap(destination, filesize);
	/* close both files */
	close(outputFile);
	close(inputFile);
	printf("copy done.\n");

}
/* driver function */
int main(int argc, char* argv[])
{
	/* parse command line arguments */
	int option = 0;
	/* 0 for help , 1 for copying without read write, so copy using mmap and 2 for copying with read write */
	int usage = 2,u = -1;
	while ((option = getopt(argc, argv,"hm:")) != -1) {
		switch (option) {
			case 'h' : usage = 0;

				   break;
			case 'm' : usage = 1;

				   break;

			default: printf("invalid usage\n");
				 exit(EXIT_FAILURE);
		}
	}
	/* print for invalid usage */
	if(argc <= 2)
	{
		printf("invalid usage\n");
		exit(EXIT_FAILURE);

	}
	/* print the help info */
	if(usage == 0)
	{
		// print the help info
		printf("copy [-m] <file_name> <new_file_name> copies content of file_name to new_file_name\n");
		printf("copy [-h] prints the help information\n");
		return 0;
	}
	int arg = 0;
	if(usage == 1)
		arg = 3;
	else
		arg = 2;
	int outputFile, inputFile,dataRead;
	// output file opened or created
	if((outputFile = open(argv[arg], O_CREAT | O_APPEND | O_RDWR))==-1){
		perror("opening output file");
	}
	// lets open the input file
	inputFile = open(argv[arg-1], O_RDONLY);
	if(inputFile == -1)
		perror("opening input file");
	// now call the appropriate function 
	if(usage == 1)
	{
		/* call copy using mmap system call */
		copy_mmap(inputFile,outputFile);		
	}
	else /* call copy using read/write system calls */
		copy_read_write(inputFile,outputFile);
	return 0;
}